vcl 4.0;

import std;
import xkey;

# Default backend definition. Set this to point to your content server.


backend default {
    .host = "nginx";
    .port = "80";
}

acl purge {
  # ACL we'll use later to allow purges
  "localhost";
  "127.0.0.1";
  "10.244.0.0/16";
  "172.16.0.0/12";
}

sub vcl_recv {
    # Happens before we check if we have this in cache already.
    #
    # Typically you clean up the request here, removing cookies you don't need,
    # rewriting the request, etc.

    if (req.url ~ "^/health") {
      return (synth(200, "OK"));
    }


    set req.backend_hint = default;


    # Allow purging
    if (req.method == "PURGE") {
        if (client.ip !~ purge) {
            return (synth(403, "Forbidden"));
        }
        if (req.http.xkey-purge) { 
          set req.http.n-gone = xkey.purge(req.http.xkey-purge);
          #set req.http.n-gone = xkey.softpurge(req.http.xkey-purge);
          return (synth(200, "Invalidated "+req.http.n-gone+" objects"));
          
        } else { 
          return (purge);
        }
    }

    # All req at this point is cached for 10y
    # Remove the proxy header (see https://httpoxy.org/#mitigate-varnish)
    unset req.http.proxy;

    # Normalize the query arguments
    set req.url = std.querysort(req.url);

    # Delete all cookies 
    unset req.http.cookie;
}

sub vcl_backend_response {
    # Happens after we have read the response headers from the backend.
    #
    # Here you clean the response headers, removing silly Set-Cookie headers
    # and other mistakes your backend does.
    
    if (beresp.status >= 400) {
      set beresp.uncacheable = true;
      return (deliver);
    }

    unset beresp.http.Cache-Control;
    unset beresp.http.Set-Cookie;

    ## Allow esi 
    set beresp.do_esi = true;

    set beresp.grace = 12h;
    set beresp.keep = 12h;
    set beresp.ttl = 10y;
}




sub vcl_deliver {
    # Happens when we have all the pieces we need, and are about to send the
    # response to the client.
    #
    # You can do accounting or modifying the final object here.
    #
    if (obj.hits > 0) { # Add debug header to see if it's a HIT/MISS and the number of hits, disable when not needed
      set resp.http.MG-Cache = "HIT";
    } else {
      set resp.http.MG-Cache = "MISS";
    }
}
