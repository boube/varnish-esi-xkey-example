## Reload 

```
TIME=$(date +%s) ; varnishadm  vcl.load varnish_$TIME /etc/varnish/default.vcl ; varnishadm  vcl.use varnish_$TIME ; varnishadm "ban req.url ~ /"
```

## Cache first 
```
curl -v  localhost/art1 -o /dev/null
curl -v  localhost/art1 -o /dev/null
```

```
curl -v  localhost/art2 -o /dev/null
curl -v  localhost/art2 -o /dev/null
```

## Purge and check
```
curl -X PURGE -H 'xkey-purge: art1' localhost 
curl -v  localhost/art1 -o /dev/null
curl -v  localhost/art2 -o /dev/null
```
(art1 should be a miss, art2 should be a hit)

```
curl -X PURGE -H 'xkey-purge: rel1' localhost 
curl -v  localhost/art1 -o /dev/null
curl -v  localhost/art2 -o /dev/null
```

(both should be a miss)

## ESI
```
curl -v  "localhost/esi/index.html?$(date +%s)"
```

https://github.com/varnish/varnish-modules/blob/master/docs/vmod_xkey.rst
https://wikitech.wikimedia.org/wiki/XKey

